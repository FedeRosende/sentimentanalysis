#include <algorithm>
//#include <chrono>
#include <iostream>
#include <vector>
#include "knn.h"

using namespace std;

KNNClassifier::KNNClassifier() : KNNClassifier(0) {}

KNNClassifier::KNNClassifier(unsigned int n_neighbors)
{
    _n_neighbors = n_neighbors;
}

void KNNClassifier::fit(SparseMatrix X, Matrix y)
{
    _X = X;
    _y = y;
}


Vector KNNClassifier::predict(SparseMatrix X)
{
    auto predictions = Vector(X.rows());

    for (unsigned k = 0; k < X.rows(); ++k)
    {
        predictions(k) = predictSingle(X.innerVector(k));
    }

    return predictions;
}

double KNNClassifier::predictSingle(SparseMatrix v)
{
    // Obtener distancias numeradas por indice
    vector<indexDistance> enumeratedDistances;
    for(unsigned i = 0; i < _X.rows(); ++i)
    {
        auto diff = _X.innerVector(i) - v;
        auto distance = diff.squaredNorm();
        enumeratedDistances.push_back(make_pair(i, distance));
    }

    // Ordenar con las instancias mas cercanas al comienzo
    nth_element(
        begin(enumeratedDistances),
        begin(enumeratedDistances) + _n_neighbors,
        end(enumeratedDistances),
        [](const indexDistance& one, const indexDistance& another) {
            return one.second < another.second;
        });

    // Encontrar el valor mas cercano (conociendo la estructura de los valores objetivo)
    double positives = 0;
    for(unsigned i = 0; i < _n_neighbors; ++i)
    {
        positives += _y(enumeratedDistances[i].first, 0);
    }

    return 2 * positives > _n_neighbors ? Positive : Negative;
}
