#include <iostream>
#include "pca.h"
#include "eigen.h"

using namespace std;

PCA::PCA(unsigned int n_components, bool convergence, unsigned int n_iter){
  _n_components=n_components;
  _convergence=convergence;
  _n_iter = n_iter;
}
PCA::PCA(){}
void PCA::fit(Matrix X){
  //Creamos matriz de covarianza
  Matrix copy=X.rowwise() -  X.colwise().mean();
  copy=(copy.transpose() * copy)/(X.rows()-1);
  pair<Vector, Matrix> p = get_first_eigenvalues(copy,copy.rows(),_n_iter,1.0e-5,_convergence);
  _eigenMatrixComplete = p.second.real(); //nos guardamos una copia con todas las componentes para agilizar futuros fit
  _eigenMatrix = _eigenMatrixComplete.leftCols(_n_components);
}

void PCA::changeAlpha(unsigned int n_components){
	//Si el valor de alpha es mas chico que lo que se uso para la transformacion, no es necesario re transformar
	if(n_components <= _n_components){ 
		_eigenMatrix = _eigenMatrixComplete.leftCols(n_components);		
	}

}

MatrixXd PCA::transform(SparseMatrix X)
{
  return X*_eigenMatrix;
}
