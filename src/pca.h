#pragma once
#include "types.h"

class PCA {
public:
    PCA(unsigned int n_components, bool convergence, unsigned int n_iter);
    PCA();
    void fit(Matrix X);
    void changeAlpha(unsigned int n_components);

    Eigen::MatrixXd transform(SparseMatrix X);
private:
    unsigned int _n_components;
    unsigned int _n_iter;
    Matrix _eigenMatrix;
    Matrix _eigenMatrixComplete;
    bool _convergence;
};
