#include <algorithm>
#include <chrono>
#include <iostream>
#include "eigen.h"
#include <bits/stdc++.h>


using namespace std;


pair<double, Vector> power_iteration(const Matrix& X, unsigned num_iter, double eps)
{
    Vector b = Vector::Random(X.cols());
    double eigenvalue;

    for (unsigned i = 0; i < num_iter; ++i)
    {
        Vector vaux = X * b;
        b = vaux / vaux.norm();
    }

    Vector bTranspose = b.transpose();
    eigenvalue = (bTranspose * (X * b)).value();
    eigenvalue = eigenvalue / (bTranspose * b).value();

    return make_pair(eigenvalue, b / b.norm());
}

pair<Vector, Matrix> get_first_eigenvalues(const Matrix& X, unsigned num, unsigned num_iter, double epsilon, bool convergence)
{
    Matrix A(X);
    Vector eigvalues(num);
    Matrix eigvectors(A.rows(), num);

    for (unsigned i = 0; i < num; ++i)
    {
        pair<double, Vector> p = convergence ? power_iteration_convergence(A,num_iter,epsilon) : power_iteration(A,num_iter,epsilon);
        Matrix v1(A.rows(),1);
        Matrix v1T(1,A.rows());
        v1 = (Matrix)p.second;
        v1T = v1.transpose();
        Matrix aux (A.rows(),A.cols());
        aux = (v1 * v1T) * p.first;
        A = A - aux;
        eigvalues[i] = p.first;
        eigvectors.col(i) = p.second;

    }
    return make_pair(eigvalues, eigvectors);
}

pair<double, Vector> power_iteration_convergence(const Matrix& X, unsigned num_iter, double eps)
{
    Vector b = Vector::Random(X.cols());
    Vector iterations_total(X.cols());
    double eigenvalue;
    unsigned int iterations = 0;
    unsigned int pos = 0;
    for (unsigned i = 0; i < num_iter; ++i)
    {
        Vector vaux = X * b;
        b = vaux / vaux.norm();

        Vector bTranspose = b.transpose();
        eigenvalue = (bTranspose * (X * b)).value();
        eigenvalue = eigenvalue / (bTranspose * b).value();

        Vector zero_candidate = (X * b) - (eigenvalue*b);
        iterations++;
        if(zero_candidate.maxCoeff()<eps){
          iterations_total(pos) = iterations;
          pos++;
          i = num_iter;
        }
    }
    /*ofstream ofs("./csvs_niter/niter_2.out", ofstream::ate | ofstream::app);
    ofs << num_iter << "," << iterations << endl;*/
    return make_pair(eigenvalue, b / b.norm());
}
