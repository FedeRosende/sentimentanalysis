from __future__ import print_function
import sys
import os
import optparse
sys.path.append('notebooks')
import sentiment
import time
import pandas as pd
import csv
import copy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score, precision_score, recall_score
from scipy import sparse


def to_pos_neg(v):
	if(v):
		return "pos"
	return "neg"


#-----------------------------------------------------------------------------
#  PARA AYUDA
#
#  python3 ./src/tp2.py -h
#
#  PARA EJECUTAR
#
#	- llamando a KNN sin setear el k
#  python3 ./src/tp2.py -m 0 -d data/imdb_small.csv -o data/result/result.csv
#
#
#	- llamando a PCA con convergencia con alpha 100 k 100 y un maximo de 500 iteraciones
#  python3 ./src/tp2.py -m 1 -a 100 -i 500 -k 100 -d data/imdb_small.csv -o data/result/result.csv
#
#
# ---El directorio de salida debe existir (no asi el archivo de resultado) ---
#-----------------------------------------------------------------------------



parser = optparse.OptionParser()

parser.add_option('-m', '--method',
    action="store", dest="method",
    type="int",
    help="Metodo 0=KNN, 1=PCA con metodo de convergencia 2=PCA sin metodo de convergencia", default=0)

parser.add_option('-d', '--dataset',
    action="store", dest="dataset_path",
    help="Ruta al source dataset", default="data/imdb_small.csv")

parser.add_option('-o', '--result',
    action="store", dest="result_path",
    help="Archivo csv para guardar el resultado. Debe ser un directorio valido", default="data/result/result.csv")

parser.add_option('-k', '--neighbours',
    action="store", dest="neighbours",
    type="int",
    help="Numero de vecinos KNN. Opcional", default=130)

parser.add_option('-a', '--alpha',
    action="store", dest="alpha",
    type="int",
    help="Valor de alpha en PCA. Opcional", default=184)

parser.add_option('-i', '--iteraciones',
    action="store", dest="iter",
    type="int",
    help="Valor de iteraciones en PCA. Opcional", default=395)

parser.add_option('-e', '--experiment_dump',
    action="store", dest="exp",
    type="int",
    help="1 para el experimento de PCA. 2 para el experimento de niter. Opcional", default=0)

options, args = parser.parse_args()

if(not os.path.isfile(options.dataset_path)):
	print("Ruta a archivo de lectura invalida...")
	exit()


if(options.method > 2): #modificar si se agregan mas metodos
	print("Metodo no reconocido")
	exit()

#%cd ../data && tar -xvf *.tgz

df = pd.read_csv(options.dataset_path, index_col=0)

print("Cantidad de documentos: {}".format(df.shape[0]))


text_train = df[df.type == 'train']["review"]
label_train = df[df.type == 'train']["label"]

text_test = df[df.type == 'test']["review"]
ids_test = df[df.type == 'test'].index
label_test = df[df.type == 'test']["label"]

print("Cantidad de instancias de entrenamiento = {}".format(len(text_train)))
print("Cantidad de instancias de test = {}".format(len(text_test)))


print("Vectorizando ..")
vectorizer = CountVectorizer(max_df=0.999, min_df=0.001, max_features=1000)
vectorizer.fit(text_train)
X_train, y_train = vectorizer.transform(text_train), (label_train == 'pos').values
X_test, y_test = vectorizer.transform(text_test), (label_test == 'pos').values
print("Dimensiones: {}".format(X_train.shape[1]))

if(options.exp == 1):
	print("Copio X")
	X_train_alt = copy.deepcopy(X_train)
	X_test_alt = copy.deepcopy(X_test)

clf = sentiment.KNNClassifier(options.neighbours)
if(options.method == 0):
	print ("Iniciando KNN")
	clf.fit(X_train, y_train)

if(not(options.method == 0)): #es 1 o 2
	print("Iniciando PCA")
	start = time.time()
	pca = sentiment.PCA(options.alpha, True, options.iter) if options.method == 1 else sentiment.PCA(options.alpha, False, options.iter)
	pca.fit(X_train.toarray())
	X_test = pca.transform(X_test)
	X_train = pca.transform(X_train)
	clf.fit(X_train, y_train)
	end = time.time()
	print("Tiempo de entrenamiento: {0} s".format(end - start))


print("Empieza prediccion")
#TODO. No es la forma mas precisa de tomar tiempos
start = time.time()
y_pred = clf.predict(sparse.csr_matrix(X_test))
end = time.time()


print("Creando archivo de salida")
Dataset = {'id': ids_test,
        'result': list(map(to_pos_neg,y_pred))
        }

df = pd.DataFrame(Dataset, columns= ['id', 'result'])

df.to_csv(options.result_path,header=True,index=False)


acc = accuracy_score(y_test, y_pred)
prec = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
print("---------------------------------")
print("Tiempo: {0} s".format(end - start))
print("Accuracy: {}".format(acc))
print("Precision: {}".format(prec))
print("Recall: {}".format(recall))


if(options.exp == 2):
	print(options.iter, end=",", file=open("./csvs_niter/niter.out", "a"))
	print(acc, end=",", file=open("./csvs_niter/niter.out", "a"))
	print(prec, end=",", file=open("./csvs_niter/niter.out", "a"))
	print(recall, file=open("./csvs_niter/niter.out", "a"))

if(options.exp == 1):
	for alfa in range(190,170,-1):
		print("Cambio alpha")
		pca.changeAlpha(alfa)
		print("Transformo los X")
		X_test_tf = pca.transform(X_test_alt)
		X_train_tf = pca.transform(X_train_alt)
		print("Nuevo fit de clf")
		clf.fit(X_train_tf, y_train)
		print("Empieza prediccion")
		#TODO. No es la forma mas precisa de tomar tiempos
		y_pred = clf.predict(sparse.csr_matrix(X_test_tf))
		acc = accuracy_score(y_test, y_pred)
		prec = precision_score(y_test, y_pred)
		recall = recall_score(y_test, y_pred)
		print("Alfa: ", end="")
		print(alfa)
		print(alfa, end=",", file=open("./csvs_alpha/alpha_niter.out", "a"))
		print(acc, end=",", file=open("./csvs_alpha/alpha_niter.out", "a"))
		print(prec, end=",", file=open("./csvs_alpha/alpha_niter.out", "a"))
		print(recall, file=open("./csvs_alpha/alpha_niter.out", "a"))
