#pragma once

#include "types.h"

#define Negative 0
#define Positive 1

class KNNClassifier {
public:
    KNNClassifier();
    KNNClassifier(unsigned int n_neighbors);

    void fit(SparseMatrix X, Matrix y);

    Vector predict(SparseMatrix X);
private:
    unsigned int _n_neighbors;
    SparseMatrix _X;
    Matrix _y;

    double predictSingle(SparseMatrix v);
};
