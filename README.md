## Instrucciones para correr ejecutable

Luego de compilar desde el notebook knn.ipynb, se puede encontrar el ejecutable en ./src/tp2.py. Para ver cómo invocarlo:
```
python3 ./src/tp2.py -h
```

## Instrucciones para correr tests

Luego de compilar desde el notebook knn.ipynb, ejecutar lo siguiente:
```
./build/test/tests
```

## Instrucciones para correr experimentos de alpha y niter

Descomentar líneas 76 y 77 del archivo eigen.cpp
Ejecutar:
```
./run_exp.sh k
```
Con k = 0, se corre el experimento para niter
Con k = 1, se corre el experimento para alpha
