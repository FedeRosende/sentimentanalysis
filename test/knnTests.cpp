#include "gtest/gtest.h"
#include "../src/knn.h"

using namespace std;

typedef Eigen::Triplet<double> T;

class KnnSmallTest : public ::testing::Test {
protected:
    KnnSmallTest() {
        knn = KNNClassifier(1);
        X = SparseMatrix(3, 4);
        y = Matrix(3, 1);
    }

    void SetUp() override {
        /*
        1 0 0 0 | 1
        0 0 1 0 | 0
        0 0 2 0 | 0
        */
        vector<T> tripletList;
        tripletList.push_back(T(0,0,1));
        tripletList.push_back(T(1,2,1));
        tripletList.push_back(T(2,2,2));

        X.setFromTriplets(tripletList.begin(), tripletList.end());

        y << 1,
             0,
             0;

        knn.fit(X, y);
    }

    // void TearDown() override {}

    KNNClassifier knn;
    SparseMatrix X;
    Matrix y;
};

TEST_F(KnnSmallTest, PredictTrainData ) {
    auto expectedFirst = y(0,0);
    auto expectedSecond = y(1,0);
    auto expectedThird = y(2,0);

    auto predictions = knn.predict(X);

    ASSERT_EQ(expectedFirst, predictions(0));
    ASSERT_EQ(expectedSecond, predictions(1));
    ASSERT_EQ(expectedThird, predictions(2));
}

TEST_F(KnnSmallTest, PredictNewData ) {
     /*
    0 0 3 0 | 0
    0 0 2 1 | 0
    1 2 0 0 | 1
    */
    vector<T> tripletList;
    tripletList.push_back(T(0,2,3));
    tripletList.push_back(T(1,2,2));
    tripletList.push_back(T(1,3,1));
    tripletList.push_back(T(2,0,1));
    tripletList.push_back(T(2,1,2));

    auto newData = SparseMatrix(3, 4);
    newData.setFromTriplets(tripletList.begin(), tripletList.end());
    
    auto expectedFirst = 0;
    auto expectedSecond = 0;
    auto expectedThird = 1;

    auto predictions = knn.predict(newData);

    ASSERT_EQ(expectedFirst, predictions(0));
    ASSERT_EQ(expectedSecond, predictions(1));
    ASSERT_EQ(expectedThird, predictions(2));
}

class KnnMediumTest : public ::testing::Test {
protected:
    KnnMediumTest() {
        knn = KNNClassifier(3);
        X = SparseMatrix(6, 4);
        y = Matrix(6, 1);
    }

    void SetUp() override {
        /*
        1 0 0 0 | 1
        0 0 1 0 | 0
        0 0 2 0 | 0
        0 5 0 0 | 1
        3 3 3 3 | 1
        0 0 0 1 | 1
        */
        vector<T> tripletList;
        tripletList.push_back(T(0,0,1));
        tripletList.push_back(T(1,2,1));
        tripletList.push_back(T(2,2,2));
        tripletList.push_back(T(3,1,5));
        tripletList.push_back(T(4,0,3));
        tripletList.push_back(T(4,1,3));
        tripletList.push_back(T(4,2,3));
        tripletList.push_back(T(4,3,3));
        tripletList.push_back(T(5,3,1));

        X.setFromTriplets(tripletList.begin(), tripletList.end());

        y << 1,
             0,
             0,
             1,
             1,
             1;

        knn.fit(X, y);
    }

    // void TearDown() override {}

    KNNClassifier knn;
    SparseMatrix X;
    Matrix y;
};

TEST_F(KnnMediumTest, PredictTrainData ) {
    auto expectedFirst = y(0,0);
    auto expectedSecond = y(1,0);
    auto expectedThird = y(2,0);
    auto expectedFourth = y(3,0);
    auto expectedFifth = y(4,0);
    auto expectedSixth = y(5,0);

    auto predictions = knn.predict(X);

    ASSERT_EQ(expectedFirst, predictions(0));
    ASSERT_EQ(expectedSecond, predictions(1));
    ASSERT_EQ(expectedThird, predictions(2));
    ASSERT_EQ(expectedFourth, predictions(3));
    ASSERT_EQ(expectedFifth, predictions(4));
    ASSERT_EQ(expectedSixth, predictions(5));
}

TEST_F(KnnMediumTest, PredictNewData ) {
     /*
    0 0 3 0 | 0
    0 0 2 1 | 0
    1 2 0 0 | 1
    */
    vector<T> tripletList;
    tripletList.push_back(T(0,2,3));
    tripletList.push_back(T(1,2,2));
    tripletList.push_back(T(1,3,1));
    tripletList.push_back(T(2,0,1));
    tripletList.push_back(T(2,1,2));

    auto newData = SparseMatrix(3, 4);
    newData.setFromTriplets(tripletList.begin(), tripletList.end());
    
    auto expectedFirst = 0;
    auto expectedSecond = 0;
    auto expectedThird = 1;

    auto predictions = knn.predict(newData);

    ASSERT_EQ(expectedFirst, predictions(0));
    ASSERT_EQ(expectedSecond, predictions(1));
    ASSERT_EQ(expectedThird, predictions(2));
}