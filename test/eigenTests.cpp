#include "gtest/gtest.h"
#include "../src/eigen.h"

using namespace std;



class EigenTest : public ::testing::Test {
protected:
    EigenTest() {
        
    }

    void SetUp() override {
        Matrix vec(5,1);
        Matrix ones(5,1);
        vec << 1, 2, 3, 4, 5; 
        Matrix mat (5,5); 
        mat = vec.asDiagonal();

        ones << 1,
                1,
                1,
                1,
                1;
        ones = ones / ones.norm();
        Matrix H (5,5);
        H <<  1,0,0,0,0,
              0,1,0,0,0,
              0,0,1,0,0,
              0,0,0,1,0,
              0,0,0,0,1;
        Matrix onesT (1,5);
        onesT = ones.transpose();
        
        H = H - 2 * (ones * onesT);
        Matrix HT = H.transpose();
        M = HT * mat * H;
    }
    
    Matrix M;
    
    double eps = 1.0e-5;
    // void TearDown() override {}

    
};

TEST_F(EigenTest, PowerIteration ) {
    
    pair<double, Vector> p = power_iteration(M,10000,eps);
    ASSERT_LE(abs(p.first - 5),eps);
}

TEST_F(EigenTest, Deflacion ) {
    pair<Vector, Matrix> p = get_first_eigenvalues(M,M.rows(),10000,eps);
    for (int i = 0; i < M.rows(); ++i)
    {
        ASSERT_LE(abs(p.first(i) - (5-i)),eps); 
        //Verificamos que los autovectores sean correctos chequeando M vi = lambda vi      
        Matrix M1 = M * p.second.col(i);
        Matrix M2 = p.first(i) * p.second.col(i);
        //Chequeamos que la maxima diferencia sea menor a eps => todos son menores
        ASSERT_LE((M1 - M2).cwiseAbs().maxCoeff(),eps);
        
    }
}