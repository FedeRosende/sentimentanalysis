#!/usr/bin/env bash
TEST_TYPE=$1
if [[ $TEST_TYPE == 0 ]]; then
  echo "NumIter,Accuracy,Precision,Recall" > ./csvs_niter/niter.out
  echo "Cota,NumIterConvergencia" > ./csvs_niter/niter_2.out
  for (( i = 20; i < 600; i++ )); do
    echo $i
    python3 ./src/tp2.py -m 2 -a 174 -i $i -k 130 -e 2 -d data/imdb_small.csv -o data/result/result.csv
  done
fi

if [[ $TEST_TYPE == 1 ]]; then
  echo "Alpha,Accuracy,Precision,Recall" > ./csvs_alpha/alpha.out
  for (( a = 5; a < 250; a+=5 )); do
    python3 ./src/tp2.py -m 2 -a $a -i 50 -k 130 -e 1 -d data/imdb_small.csv -o data/result/result.csv
  done
fi
